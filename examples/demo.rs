use std::error;

use space_traders::{
    apis::{
        agents_api::{self, GetMyAgentSuccess},
        default_api::{self, RegisterSuccess},
        fleet_api::{self, GetMyShipsSuccess},
        systems_api::{self, GetWaypointSuccess},
        Configuration,
    },
    models::{
        Agent, FactionSymbol, GetMyAgent200Response, GetMyShips200Response, GetWaypoint200Response,
        Register201Response, RegisterRequest, Waypoint,
    },
};

#[tokio::main]
async fn main() {
    let maybe_token = std::env::args()
        .nth(1)
        .expect("should have a call sign or token as argument");

    // Use token or start a new game.
    let game = match Game::from_token(&maybe_token).await {
        Ok(game) => game,
        Err(_) if maybe_token.len() < 20 => {
            // The user actually gave a call sign to generate a new agent with.
            let call_sign = maybe_token;

            println!("Call sign: {}", call_sign);

            // Generate new agent.
            Game::new(&call_sign, FactionSymbol::Cosmic).await.unwrap()
        }
        Err(err) => panic!("{}", err),
    };

    // Get the agent.
    println!("Headquarters: {}", game.agent().await.unwrap().headquarters);

    // Get the starting location.
    let waypoint = game.starting_location().await.unwrap();
    println!("Ship location: {}, {}", waypoint.x, waypoint.y);
}

pub struct Game {
    configuration: Configuration,
    system: String,
    waypoint: String,
}

impl Game {
    pub async fn new(
        call_sign: &str,
        faction: FactionSymbol,
    ) -> Result<Self, Box<dyn error::Error>> {
        let mut configuration = Configuration::new();

        // Try to register a new agent.
        let response = default_api::register(
            &configuration,
            Some(RegisterRequest {
                faction,
                symbol: call_sign.to_owned(),
                email: None,
            }),
        )
        .await?;

        let RegisterSuccess::Status201(Register201Response { data }) = response.content;

        println!();
        println!("Do not loose this token if you want to keep your agent!!!");
        println!("Token: {}", data.token);
        println!();

        // This is the agent's "password" for future api calls.
        configuration.bearer_access_token = Some(data.token);

        Ok(Self {
            configuration,
            system: data.ship.nav.system_symbol,
            waypoint: data.ship.nav.waypoint_symbol,
        })
    }

    pub async fn from_token(token: &str) -> Result<Self, Box<dyn error::Error>> {
        let mut configuration = Configuration::new();
        configuration.bearer_access_token = Some(token.to_owned());

        let mut game = Self {
            configuration,
            system: String::new(),
            waypoint: String::new(),
        };

        match game.agent().await {
            Ok(_) => {
                // The agent exists!.

                // The the agent's ships.
                let response = fleet_api::get_my_ships(&game.configuration, None, None)
                    .await
                    .unwrap();

                let GetMyShipsSuccess::Status200(GetMyShips200Response { data, .. }) =
                    response.content;

                // Get the location of the first one.
                game.system = data[0].nav.system_symbol.clone();
                game.waypoint = data[0].nav.waypoint_symbol.clone();

                Ok(game)
            }
            Err(err) => Err(err),
        }
    }

    pub async fn agent(&self) -> Result<Agent, Box<dyn error::Error>> {
        let response = agents_api::get_my_agent(&self.configuration).await?;

        let GetMyAgentSuccess::Status200(GetMyAgent200Response { data }) = response.content;

        Ok(data)
    }

    pub async fn starting_location(&self) -> Result<Waypoint, Box<dyn error::Error>> {
        let response =
            systems_api::get_waypoint(&self.configuration, &self.system, &self.waypoint).await?;

        let GetWaypointSuccess::Status200(GetWaypoint200Response { data }) = response.content;

        Ok(data)
    }
}
