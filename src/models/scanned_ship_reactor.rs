//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

/// The reactor of the ship.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ScannedShipReactor {
    /// The symbol of the reactor.
    #[serde(rename = "symbol")]
    pub symbol: String,
}

impl ScannedShipReactor {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(symbol: String) -> ScannedShipReactor {
        ScannedShipReactor { symbol }
    }
}
