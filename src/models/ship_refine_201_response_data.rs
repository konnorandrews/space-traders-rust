//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

///
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ShipRefine201ResponseData {
    #[serde(rename = "cargo")]
    pub cargo: crate::models::ShipCargo,
    #[serde(rename = "cooldown")]
    pub cooldown: crate::models::Cooldown,
    /// Goods that were produced by this refining process.
    #[serde(rename = "produced")]
    pub produced: Vec<crate::models::ShipRefine201ResponseDataProducedInner>,
    /// Goods that were consumed during this refining process.
    #[serde(rename = "consumed")]
    pub consumed: Vec<crate::models::ShipRefine201ResponseDataProducedInner>,
}

impl ShipRefine201ResponseData {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        cargo: crate::models::ShipCargo,
        cooldown: crate::models::Cooldown,
        produced: Vec<crate::models::ShipRefine201ResponseDataProducedInner>,
        consumed: Vec<crate::models::ShipRefine201ResponseDataProducedInner>,
    ) -> ShipRefine201ResponseData {
        ShipRefine201ResponseData {
            cargo,
            cooldown,
            produced,
            consumed,
        }
    }
}
