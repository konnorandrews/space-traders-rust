//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

/// The frame of the ship.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ScannedShipFrame {
    /// The symbol of the frame.
    #[serde(rename = "symbol")]
    pub symbol: String,
}

impl ScannedShipFrame {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(symbol: String) -> ScannedShipFrame {
        ScannedShipFrame { symbol }
    }
}
