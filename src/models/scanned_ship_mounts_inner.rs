//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

/// A mount on the ship.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ScannedShipMountsInner {
    /// The symbol of the mount.
    #[serde(rename = "symbol")]
    pub symbol: String,
}

impl ScannedShipMountsInner {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(symbol: String) -> ScannedShipMountsInner {
        ScannedShipMountsInner { symbol }
    }
}
