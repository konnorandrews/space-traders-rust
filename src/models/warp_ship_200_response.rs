//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

///
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct WarpShip200Response {
    #[serde(rename = "data")]
    pub data: crate::models::WarpShip200ResponseData,
}

impl WarpShip200Response {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(data: crate::models::WarpShip200ResponseData) -> WarpShip200Response {
        WarpShip200Response { data }
    }
}
