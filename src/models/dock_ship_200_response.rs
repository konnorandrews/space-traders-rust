//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

///
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct DockShip200Response {
    #[serde(rename = "data")]
    pub data: crate::models::OrbitShip200ResponseData,
}

impl DockShip200Response {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(data: crate::models::OrbitShip200ResponseData) -> DockShip200Response {
        DockShip200Response { data }
    }
}
