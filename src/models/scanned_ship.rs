//! Generated by: <https://openapi-generator.tech>
//!
//! Version of specification: `2.0.0`

use serde::{Deserialize, Serialize};

/// The ship that was scanned. Details include information about the ship that could be detected by the scanner.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ScannedShip {
    /// The globally unique identifier of the ship.
    #[serde(rename = "symbol")]
    pub symbol: String,
    #[serde(rename = "registration")]
    pub registration: crate::models::ShipRegistration,
    #[serde(rename = "nav")]
    pub nav: crate::models::ShipNav,
    #[serde(rename = "frame", skip_serializing_if = "Option::is_none")]
    pub frame: Option<crate::models::ScannedShipFrame>,
    #[serde(rename = "reactor", skip_serializing_if = "Option::is_none")]
    pub reactor: Option<crate::models::ScannedShipReactor>,
    #[serde(rename = "engine")]
    pub engine: crate::models::ScannedShipEngine,
    /// List of mounts installed in the ship.
    #[serde(rename = "mounts", skip_serializing_if = "Option::is_none")]
    pub mounts: Option<Vec<crate::models::ScannedShipMountsInner>>,
}

impl ScannedShip {
    /// Create value with optional fields set to `None`.
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        symbol: String,
        registration: crate::models::ShipRegistration,
        nav: crate::models::ShipNav,
        engine: crate::models::ScannedShipEngine,
    ) -> ScannedShip {
        ScannedShip {
            symbol,
            registration,
            nav,
            frame: None,
            reactor: None,
            engine,
            mounts: None,
        }
    }
}
