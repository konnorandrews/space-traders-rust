//! This is an async Rust client for the [SpaceTraders API](https://spacetraders.io/) game. This
//! client is not official as are all client libraries for the game.
//!
//! The `apis` and `models` modules are automatically generated from the bundled form of [the specification](https://github.com/SpaceTradersAPI/api-docs) that is available on the [Stoplight workspace](https://spacetraders.stoplight.io/).
//!
//! This crate is generated for version `2.0.0` of the game.

pub mod apis;
pub mod models;
